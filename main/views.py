from urllib import response
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
# from main.serializer import UserModelSerializer
from django.views.decorators.csrf import csrf_exempt
from .models import *


def index(request):
    return JsonResponse({'message':'this is law cache task'})

def validateRequest(request, listOfAttrs):
    for atr in listOfAttrs:
        data = request.data.get(atr)
        if data is None:
            return JsonResponse({'error': "{} is required".format(atr)}, status=status.HTTP_400_BAD_REQUEST)
    return True

@csrf_exempt
@api_view(['POST'])
def add_user(request):
    isRequestValid = validateRequest(request,['nama','npm'])
    if isRequestValid != True:
        return isRequestValid
    name = request.data.get('nama')
    npm = request.data.get('npm')
    UserModel.objects.create(
        name = name,
        npm = npm
    )
    return JsonResponse({'status': 'OK'},status = status.HTTP_200_OK)